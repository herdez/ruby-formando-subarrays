## Ejercicio - Formando subarrays

Define el método `subarray_index` que recibe como argumento un `array` de `letras`, así como un `factor` y deberá regresar un nuevo arreglo como el siguiente:

```ruby
#Driver code

p subarray_index(["c", "b", "a"], 2) == [["c", [1, 2]], ["b", [1, 2]], ["a", [1, 2]]]
p subarray_index(["a"], 3) == [["a", [1, 2, 3]]]
```
